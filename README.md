# openflexure-microscope-keyclient

Local or remote keyboard control for an OpenFlexure microscope server.

## As of March 2019, this project is deprecated in favour of [OpenFlexure eV](https://gitlab.com/openflexure/openflexure-microscope-jsclient).

If you are in desperate need of a CLI client for the microscope, please open an issue, and we can consider restarting development. Thanks.