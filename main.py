import argparse
import numpy as np
from pprint import pprint
import time

from keyboardinterface.base import KeyboardInterface
from api.base import APIconnection

# Debugging
import random

# Create a keyboard interface
interface = KeyboardInterface(exitkeys=['\x1b', '\x1b\x1b'], cmdkey='#')

# Menu methods
def draw_menu(name, value, connection):
    if name:
        message = "{}: {}".format(name, value)
    else:
        message = ""
    connection.set_overlay(message=message)

def get_from_key(connection, key, picamera=False, value_dp=None):
    conf = connection.get_config()
    if picamera:
        conf = conf['picamera_params']

    if key in conf:
        value = conf[key]
    else:
        value = None

    # Round value if valid argument
    if type(value_dp) is int:
        value = round(value, value_dp)

    return value

def set_from_key(value, connection=None, key=None, picamera=False, value_dp=None):

    # Round value if valid argument
    if type(value_dp) is int:
        value = round(value, value_dp)

    conf = {'picamera_params': {}}  # Blank config

    if picamera:
        target = conf['picamera_params']
    else:
        target = conf

    target[key] = value

    return connection.set_config(conf)

def get_zoom(connection=None):
    response = connection.get_zoom()
    return float(response["zoom_value"])

def set_zoom(value, connection=None):
    value = round(value, 2)  # Round to 2dp

    response = connection.set_zoom(zoom_value=value)
    return float(response["zoom_value"])

def get_stepsize(connection=None, axis='xy'):
    if axis in connection.velocity:
        return connection.velocity[axis]
    else:
        return 0

def set_stepsize(value, connection=None, axis='xy'):
    connection.velocity[axis] = value
    return value

def new_capture(connection):
    overlay_dict = connection.get_overlay()  # Store current overlay

    connection.set_overlay(message="")  # Clear overlay for capture
    cap = connection.new_capture(use_video_port=False, keep_on_disk=True)
    
    connection.set_overlay(message="Captured to\n{}".format(cap['path']))
    time.sleep(1.5)
    connection.set_overlay(message=overlay_dict['text'], size=overlay_dict['size'])

def test_method(connection=None):
    print("Running some method on {}".format(connection))

# Methods
def show_config(connection):
    pprint(connection.get_config())

def show_state(connection):
    pprint(connection.get_state())

# Bind all interface to an API connection
def bind_keys(interface, connection):
    interface.cmd('state', show_state, connection=connection)
    interface.cmd('config', show_config, connection=connection)
    interface.cmd('q', exit)

    # Capture
    interface.key('j', new_capture, connection=connection)

    # GPU preview
    interface.key('v', connection.start_preview)
    interface.key('b', connection.stop_preview)

    # x-y movement
    interface.key('w', connection.move_by, y=1)
    interface.key('s', connection.move_by, y=-1)
    interface.key('d', connection.move_by, x=-1)
    interface.key('a', connection.move_by, x=1)

    # z movement
    interface.key('q', connection.move_by, z=-1)
    interface.key('e', connection.move_by, z=1)

def build_menu(interface, connection):
    interface.set_menu_onchange(draw_menu, connection=connection)

    interface.add_menu_value(
        'name', 
        get_from_key, 
        connection=connection, 
        key="name", 
        picamera=False)

    interface.add_menu_setting(
        'zoom', 
        0.1, 
        set_zoom, 
        get_zoom, 
        connection=connection)

    interface.add_menu_setting(
        'xy_velocity', 
        10, 
        set_stepsize, 
        get_stepsize, 
        connection=connection,
        axis='xy')

    interface.add_menu_setting(
        'z_velocity', 
        10, 
        set_stepsize, 
        get_stepsize, 
        connection=connection,
        axis='z')

    interface.add_menu_setting(
        'shutter_speed', 
        200, 
        set_from_key, 
        get_from_key, 
        connection=connection, 
        key="shutter_speed", 
        picamera=True,)
    
    interface.add_menu_setting(
        'digital_gain', 
        0.1, 
        set_from_key, 
        get_from_key, 
        connection=connection, 
        key="digital_gain", 
        picamera=False,
        value_dp=3)

    interface.add_menu_setting(
        'analog_gain', 
        0.1, 
        set_from_key, 
        get_from_key, 
        connection=connection, 
        key="analog_gain", 
        picamera=False,
        value_dp=3)
    
    interface.add_menu_function(
        'test_function',
        test_method,
        connection=connection
    )

# Create interface
if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--address', '-a', help="Network address of microscope host", type=str, default="localhost")
    parser.add_argument('--port', '-p', help="Port of microscope server", type=int, default=5000)

    args = parser.parse_args()

    connection = APIconnection(host=args.address, port=args.port)
    print("Connecting to {}...".format(connection.base))
    try:
        conn_conf = connection.get_config()
    except:
        print("Connection could not be established. Exiting...")
        exit()
    
    print("Connected to {}".format(conn_conf['name']))

    print("Binding interface...")
    bind_keys(interface, connection)

    print("Building menu...")
    build_menu(interface, connection)

    print("Interface started!")
    interface.start()

    print("Exiting...")
    draw_menu(None, None, connection)
    connection.stop_preview()