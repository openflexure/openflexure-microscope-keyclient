from readchar import readchar, readkey
import sys
import logging

class KeyboardInterface:
    def __init__(self, exitkeys=['x'], cmdkey='#'):
        self.keydict = {}  # Store map of keys
        self.keykwargs = {}  # Store any associated kwargs

        self.cmddict = {}
        self.cmdkwargs = {}

        self.menu = []
        self.menu_index = 0

        self.menu_method = {
            'method': None,
            'kwargs': []
        }

        self.exitkeys = exitkeys
        self.cmdkey = cmdkey

    def do_nothing(self, *args, **kwargs):
        pass

    def noscroll(self, *args, **kwargs):
        return "(Enter to run)"

    def clearline(self, *args, **kwargs):
        sys.stdout.write("\033[F")  # back to previous line
        sys.stdout.write("\033[K")  # clear line

    def bind(self, cmd_dict, kw_dict, cmd, function, **kwargs):
        if not cmd in cmd_dict:
            cmd_dict[cmd] = function
            kw_dict[cmd] = kwargs
            logging.debug("Attaching {} to {}({})".format(cmd, cmd_dict[cmd], kw_dict[cmd]))

    def key(self, key_id, function, **kwargs):
        self.bind(self.keydict, self.keykwargs, key_id, function, **kwargs)

    def cmd(self, cmd, function, **kwargs):
        self.bind(self.cmddict, self.cmdkwargs, cmd, function, **kwargs)

    def add_menu_item(self, name, step, setvalue, getvalue, scrollable, **kwargs):
        item = {
            'scrollable': scrollable, 
            'name': name,
            'step': step,  # Magnitude of step size
            'setvalue': setvalue,  # Function to call on change (value={set value}, **kwargs)
            'getvalue': getvalue,  # Function to return current value (**kwargs)
            'kwargs': kwargs
        }

        self.menu.append(item)

    def add_menu_setting(self, name, step, setvalue, getvalue, **kwargs):
        self.add_menu_item(name, step, setvalue, getvalue, True, **kwargs)

    def add_menu_value(self, name, getvalue, **kwargs):
        self.add_menu_item(name, None, self.clearline, getvalue, False, **kwargs)

    def add_menu_function(self, name, function, **kwargs):
        self.add_menu_item(name, None, function, self.noscroll, False, **kwargs)

    def set_menu_onchange(self, function, **kwargs):
        """
        Add a function to be called when the current menu item changes
        """
        self.menu_method['method'] = function
        self.menu_method['kwargs'] = kwargs

    def menu_onchange(self, name, value):
        """
        Call functions  when the current menu item changes. Arguments (name, value) passed
        """
        if self.menu_method['method']:
            self.menu_method['method'](name, value, **self.menu_method['kwargs'])

    def start(self):
        while True:
            c = readkey()

            # Exit
            if c in self.exitkeys:
                break

            # Console
            elif c == self.cmdkey:
                cmd = input("\n>>> ")
                if cmd in self.cmddict:
                    self.cmddict[cmd](**self.cmdkwargs[cmd])

            # Menu keys
            elif c in ['[', ']', '-', '_', '=', '+', '\r']:
                if c in ['[', ']']: # scroll through parameters
                    N = len(self.menu) + 1  # +1 because our index-0 corresponds to no item selected
                    d = 1 if c == ']' else -1
                    self.menu_index = (self.menu_index + N + d) % N

                if self.menu_index > 0:
                    # Get menu item properties, and current value
                    scrollable = self.menu[self.menu_index-1]['scrollable']
                    name = self.menu[self.menu_index-1]['name']
                    step = self.menu[self.menu_index-1]['step']
                    get_method = self.menu[self.menu_index-1]['getvalue']
                    set_method = self.menu[self.menu_index-1]['setvalue']
                    kwargs = self.menu[self.menu_index-1]['kwargs']

                    value = get_method(**kwargs)

                    # If menu item is a scrollable numeric setting
                    if scrollable:
                        if c in ['+', '=']:
                            set_method(value+step, **kwargs)
                            value = get_method(**kwargs)
                        elif c in ['-', '_']:
                            set_method(value-step, **kwargs)
                            value = get_method(**kwargs)

                    # If menu item is a function or static value
                    else:
                        if c == '\r':
                            set_method(**kwargs)

                elif self.menu_index == 0:
                    name = ""
                    value = ""

                # Rewrite line unless running a function
                if not (c == '\r'):
                    self.clearline()

                print("{}: {}".format(name, value))
                self.menu_onchange(name, value)

            # Action keys
            elif c in self.keydict:
                logging.debug("{} pressed".format(c))
                self.keydict[c](**self.keykwargs[c])
