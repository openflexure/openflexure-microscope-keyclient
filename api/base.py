import requests

class APIconnection:
    def __init__(self, host="localhost", port=5000, api_ver="v1", velocity_xy=75, velocity_z=40):
        self.base = self.build_base(host=host, port=port, api_ver=api_ver)

        self.velocity = {
            'xy': velocity_xy,
            'z': velocity_z,
        }

    def build_base(self, host, port, api_ver):
        return "http://{}:{}/api/{}".format(host, port, api_ver)

    def uri(self, suffix):
        return self.base + suffix

    def get(self, route, timeout=5):
        r = requests.get(self.uri(route), timeout=timeout)
        return r.json()

    def post(self, route, json=None, timeout=5):
        r = requests.post(self.uri(route), json=json, timeout=timeout)
        return r.json()

    def set_overlay(self, message="", size=50):
        json = {
            "text": message,
            "size": size
        }
        return self.post('/camera/overlay', json=json)

    def get_overlay(self):
        return self.get('/camera/overlay')

    def get_config(self):
        return self.get('/config')

    def set_config(self, config_dict):
        return self.post('/config', json=config_dict)

    def get_state(self):
        return self.get('/state')

    def start_preview(self):
        return self.post('/camera/preview/start')

    def stop_preview(self):
        return self.post('/camera/preview/stop')

    def move_by(self, x=0, y=0, z=0):
        json = {
            "x": self.velocity['xy']*x,
            "y": self.velocity['xy']*y,
            "z": self.velocity['z']*z
        }
        return self.post('/stage/position', json=json)

    def new_capture(self, use_video_port=True, keep_on_disk=False):
        json = {
            "keep_on_disk": keep_on_disk,
            "use_video_port": use_video_port
        }
        return self.post('/camera/capture', json=json)

    def set_zoom(self, zoom_value=1.0):
        json = {
            "zoom_value": zoom_value,
        }
        return self.post('/camera/zoom', json=json)

    def get_zoom(self):
        return self.get('/camera/zoom')